import os
from subprocess import call

# Current directory
sVersion = "1.0"
sCurrentDir = os.getcwd();
print("Your are working in: "+sCurrentDir);

# Scripty name (not used)
# sScriptName = os.path.basename(__file__);

# Get the list of dir
lDir = os.listdir(".");
iError = 0
lError = []
		
os.system("cls")
print("|===========================|")
print("|  GIT UPDATE - Yves Lange  |")
print("|  Version "+sVersion+"              |")
print("|===========================|\n\n")
for dir in lDir:

  if(os.path.isdir(dir)):
    
    
    sRootDir = sCurrentDir+"\\"+dir;
    id = os.system("cd "+sRootDir+" && git status > nul")
    if(id == 0 and id != 128):
      
      print("<================================================>")
      print("$ Trying to update: "+dir)
      id2 = os.system("cd "+sRootDir+" && git pull > nul")
      if(id2 != 0):
        iError+=1
        lError.append(dir);
        print("$ Failed to update: "+dir)
      else:
        print("$ Successfully update: "+dir)
    
      print("<================================================>\n\n")

# Showing all errors      
if(iError > 0): 
  print("\n\n\n____________________________________")
  print("ERRORS: "+str(iError))
  for s in lError:
    print("> "+s)

    

